package com.vincentlenair.interview.cats.service;

import com.vincentlenair.interview.cats.client.CatApiClient;
import com.vincentlenair.interview.cats.model.BreedDTO;
import com.vincentlenair.interview.cats.model.FactDTO;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public class CatService {

    private final CatApiClient client;

    public CatService(final CatApiClient client) {
        this.client = client;
    }

    public Flux<FactDTO> getFacts(){
        return client.findAllFacts();
    }


    public Flux<BreedDTO> getBreeds() {
        return client.findAllBreeds();
    }


}
