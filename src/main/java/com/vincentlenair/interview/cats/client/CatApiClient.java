package com.vincentlenair.interview.cats.client;

import com.vincentlenair.interview.cats.model.BreedDTO;
import com.vincentlenair.interview.cats.model.BreedResponseDTO;
import com.vincentlenair.interview.cats.model.FactDTO;
import com.vincentlenair.interview.cats.model.FactResponseDTO;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

@Component
public class CatApiClient {
    private final WebClient webClient;

    public CatApiClient(final WebClient.Builder webBuilder) {
        this.webClient = webBuilder.baseUrl("https://catfact.ninja").build();
    }

    public Flux<FactDTO> findAllFacts() {
        return webClient.get()
            .uri(builder -> builder.path("/facts").queryParam("limit", 1000).build())
            .retrieve()
            .bodyToMono(FactResponseDTO.class)
            .flatMapIterable(FactResponseDTO::getData);
    }

    public Flux<BreedDTO> findAllBreeds() {
        return webClient.get()
            .uri(builder -> builder.path("/breeds").build())
            .retrieve()
            .bodyToMono(BreedResponseDTO.class)
            .flatMapIterable(BreedResponseDTO::getData);
    }
}
