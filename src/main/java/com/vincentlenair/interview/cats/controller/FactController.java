package com.vincentlenair.interview.cats.controller;

import com.vincentlenair.interview.cats.model.FactDTO;
import com.vincentlenair.interview.cats.service.CatService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping(path = "/facts")
public class FactController {

    private final CatService catService;

    public FactController(final CatService catService) {
        this.catService = catService;
    }

    @GetMapping(path = "")
    public Flux<FactDTO> getCatFacts() {
        return catService.getFacts();
    }
}
