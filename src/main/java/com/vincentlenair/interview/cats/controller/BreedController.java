package com.vincentlenair.interview.cats.controller;

import com.vincentlenair.interview.cats.service.CatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.vincentlenair.interview.cats.model.BreedDTO;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping(path = "/breeds")
public class BreedController {
    private final CatService catService;

    public BreedController(final CatService catService) {
        this.catService = catService;
    }

    @GetMapping(path = "")
    public Flux<BreedDTO> getCatBreeds() {
        return catService.getBreeds();
    }
}
