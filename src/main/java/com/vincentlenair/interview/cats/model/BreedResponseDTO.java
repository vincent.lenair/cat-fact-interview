package com.vincentlenair.interview.cats.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public record BreedResponseDTO(@JsonProperty List<BreedDTO> data) {

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public BreedResponseDTO(@JsonProperty("data") final List<BreedDTO> data) {
        this.data = data;
    }

    public List<BreedDTO> getData() {
        return data;
    }
}
