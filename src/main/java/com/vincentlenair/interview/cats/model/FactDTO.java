package com.vincentlenair.interview.cats.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public record FactDTO(@JsonProperty String fact) {
    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public FactDTO(@JsonProperty("fact") final String fact) {
        this.fact = fact;
    }

    @Override
    public String fact() {
        return fact;
    }
}
