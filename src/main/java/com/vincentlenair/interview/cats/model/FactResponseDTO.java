package com.vincentlenair.interview.cats.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public record FactResponseDTO(@JsonProperty List<FactDTO> data) {

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public FactResponseDTO(@JsonProperty("data") final List<FactDTO> data) {
        this.data = data;
    }

    public List<FactDTO> getData() {
        return data;
    }
}
