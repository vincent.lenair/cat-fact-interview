package com.vincentlenair.interview.cats.controller;

import com.vincentlenair.interview.cats.CatFactApiApplication;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureWebTestClient
@SpringBootTest(
    classes = { CatFactApiApplication.class },
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FactControllerTest {

    @Autowired
    private WebTestClient webTestClient;


    @Test
    public void shouldReturnHttp200OnSuccess() {
        webTestClient.get()
            .uri("/facts")
            .exchange()
            .expectStatus()
            .isOk();
    }
}