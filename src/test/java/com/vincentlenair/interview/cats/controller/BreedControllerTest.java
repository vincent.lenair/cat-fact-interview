package com.vincentlenair.interview.cats.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import com.vincentlenair.interview.cats.CatFactApiApplication;
import org.springframework.test.web.reactive.server.WebTestClient;

@AutoConfigureWebTestClient
@SpringBootTest(
        classes = { CatFactApiApplication.class },
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BreedControllerTest {

    @Autowired
    private WebTestClient webTestClient;

    @Test
    public void shouldReturnHttp200OnSuccess() {
        webTestClient.get()
            .uri("/breeds")
            .exchange()
            .expectStatus()
            .isOk();
    }
}